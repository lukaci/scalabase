object var_val {
	println("the very basics")                //> the very basics
	
	/*
   * base field declarations
	 * val & var
	 */
	
	val n : Int = 0                           //> n  : Int = 0
	
	// n = 1 // -> error, val reassignment
	
	var vn : Int = 0                          //> vn  : Int = 0
	
	vn = 1 // -> ok
 
  vn                                              //> res0: Int = 1
  
  
 
	/*
	 * type inference
	 */
	
	val n2 = 0 // n2 is assigned to type Int automatically
                                                  //> n2  : Int = 0
	
	var vn2 = 0 // vn2 is assigned to type Int automatically
                                                  //> vn2  : Int = 0
	
	println("try to avoid using 'var'!!")     //> try to avoid using 'var'!!
}