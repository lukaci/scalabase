import java.util.Random

object seq {
	println("seq basics")                     //> seq basics

	/*
	 * ok, cool theory
	 *
	 * but in real world?
	 */
	
	/*
	 * in most cases we have to deal with data, collections of data
	 * here is a strong aspect of scala: a powerful and efficient implementation of immutable collections and its comprehension
	 */
	 
	/*
	 * numeric range example
	 */
	val r = 0 to 9                            //> r  : scala.collection.immutable.Range.Inclusive = Range(0, 1, 2, 3, 4, 5, 6,
                                                  //|  7, 8, 9)
	
	/*
	 * O_O
 	 */
	
	val r2 = 0 to 19 by 2                     //> r2  : scala.collection.immutable.Range = Range(0, 2, 4, 6, 8, 10, 12, 14, 16
                                                  //| , 18)
	
	
	/*
	 * list example
	 */
	
	val l = List(1, 2, 3, 4)                  //> l  : List[Int] = List(1, 2, 3, 4)
	
	
	/*
	 * because collections are immutable, every modification on a collection generates a new instance of that collection, modified as requested
	 *
	 * ... but the overhead is not so high as you may think! internal implementation, based on object immutability, allowed the implementators
	 * to perform some optimizations, so collection contents are never duplicated, and only some collection 'infrastructure' is added
	 *
	 * the user should never be interested of, for example, if the 'tail' method returns every time a new instance of a sublist of items, or
	 *  the same instance everytime he calls: the list is immutable, the tail is immutable too
	 *
	 * the immutability brings some discounts, which helps us to program better in a functional way
	 */
	
	/*
	 * because of its immutable form, collections are often accessed by head and tail
	 *
	 * - head is the first element of the collection
	 * - tail is the rest
	 */

	l.head                                    //> res0: Int = 1
	
	l.tail                                    //> res1: List[Int] = List(2, 3, 4)


	/*
	 * there is a special value in collections, which represents the empty list : Nil
	 */
	 
	val empty = List()                        //> empty  : List[Nothing] = List()
	empty == Nil                              //> res2: Boolean = true
	
	/*
	 * a list ALWAYS terminates with Nil
	 */
	
	/*
	 * special operators: lists are full of special concatenation operators
	 */
	 
	val lst = 1 :: 2 :: 3 :: Nil              //> lst  : List[Int] = List(1, 2, 3)
	
	
	val lst2 = 4 :: 5 :: 6 :: Nil             //> lst2  : List[Int] = List(4, 5, 6)
	
	 
	lst ++ lst2                               //> res3: List[Int] = List(1, 2, 3, 4, 5, 6)
	
	lst :+ 100                                //> res4: List[Int] = List(1, 2, 3, 100)
	
	(-100) +: lst                             //> res5: List[Int] = List(-100, 1, 2, 3)
	
	
	
	/*
	 * subsequencing
	 */
	 
	 lst.drop(1).take(2)                      //> res6: List[Int] = List(2, 3)
	 
	 lst.slice(1, 4)                          //> res7: List[Int] = List(2, 3)
	 
	 
	 
	/*
	 * lazy transformations
   */
	  
	lst.map(v => v * v)                       //> res8: List[Int] = List(1, 4, 9)
	
	
	
	/*
	 * filtering
	 */
	lst.filter(v => v > 2)                    //> res9: List[Int] = List(3)
	
	
	/*
	 * flattening
	 *
	 * list flattening seems not so common, but you'll use very much!
	 * it comes out when doing nested 'cycles'
	 */
	 
	val lstOfLst = List(List(1,2), List(3, 4), List(5,6))
                                                  //> lstOfLst  : List[List[Int]] = List(List(1, 2), List(3, 4), List(5, 6))
	 
	lstOfLst.flatten                          //> res10: List[Int] = List(1, 2, 3, 4, 5, 6)
	 
	 
	 
	/*
	 * flatMap : if we have a mapping function which expands each element to another sequence, we can use flatMap
	 *   then to reconvert the obtainet List(List(.... into a flat list
	 */
	 
	val dozens = 0 to 50 by 10                //> dozens  : scala.collection.immutable.Range = Range(0, 10, 20, 30, 40, 50)
	
	dozens.map(v => v to (v + 5) by 5)        //> res11: scala.collection.immutable.IndexedSeq[scala.collection.immutable.Ran
                                                  //| ge] = Vector(Range(0, 5), Range(10, 15), Range(20, 25), Range(30, 35), Rang
                                                  //| e(40, 45), Range(50, 55))
	
	dozens.map(v => v to (v + 5) by 5).flatten//> res12: scala.collection.immutable.IndexedSeq[Int] = Vector(0, 5, 10, 15, 20
                                                  //| , 25, 30, 35, 40, 45, 50, 55)
                                                  
	dozens.flatMap(v => v to (v + 5) by 5)    //> res13: scala.collection.immutable.IndexedSeq[Int] = Vector(0, 5, 10, 15, 20
                                                  //| , 25, 30, 35, 40, 45, 50, 55)
	 
	/*
	 * in general, if we have more nested functions which expands each item to a sequence, and our result is a flat list of items
	 *   obtained by the innermost collection, we can write it as follows:
	 *
	 *  make N the number of nested maps
	 *
	 *    -> there are N - 1 flatMaps (the first N-1)
	 *    -> there is 1 map (the last)
	 */
	 
	(1 to 5).flatMap(v0 =>
		(1 to 5).flatMap(v1 =>
			(1 to 5).map(v2 => (v0, v1, v2))))
                                                  //> res14: scala.collection.immutable.IndexedSeq[(Int, Int, Int)] = Vector((1,1
                                                  //| ,1), (1,1,2), (1,1,3), (1,1,4), (1,1,5), (1,2,1), (1,2,2), (1,2,3), (1,2,4)
                                                  //| , (1,2,5), (1,3,1), (1,3,2), (1,3,3), (1,3,4), (1,3,5), (1,4,1), (1,4,2), (
                                                  //| 1,4,3), (1,4,4), (1,4,5), (1,5,1), (1,5,2), (1,5,3), (1,5,4), (1,5,5), (2,1
                                                  //| ,1), (2,1,2), (2,1,3), (2,1,4), (2,1,5), (2,2,1), (2,2,2), (2,2,3), (2,2,4)
                                                  //| , (2,2,5), (2,3,1), (2,3,2), (2,3,3), (2,3,4), (2,3,5), (2,4,1), (2,4,2), (
                                                  //| 2,4,3), (2,4,4), (2,4,5), (2,5,1), (2,5,2), (2,5,3), (2,5,4), (2,5,5), (3,1
                                                  //| ,1), (3,1,2), (3,1,3), (3,1,4), (3,1,5), (3,2,1), (3,2,2), (3,2,3), (3,2,4)
                                                  //| , (3,2,5), (3,3,1), (3,3,2), (3,3,3), (3,3,4), (3,3,5), (3,4,1), (3,4,2), (
                                                  //| 3,4,3), (3,4,4), (3,4,5), (3,5,1), (3,5,2), (3,5,3), (3,5,4), (3,5,5), (4,1
                                                  //| ,1), (4,1,2), (4,1,3), (4,1,4), (4,1,5), (4,2,1), (4,2,2), (4,2,3), (4,2,4)
                                                  //| , (4,2,5), (4,3,1), (4,3,2), (4,3,3), (4,3,4), (4,3,5), (4,4,1), (4,4,2), (
                                                  //| 4,4,3), (4,4,4), (4,4,5
                                                  //| Output exceeds cutoff limit.
	

	/*
	 * syntactic sugar: somethings that adds 'beauty' to our code, but not adding functionality
	 *
	 * scala automatically converts the 'for' iterations to a sequence of map / flat map / filter ...
	 *
	 * previous example can be rewritten as follows:
	 */
	
	 for {
	 	v0 <- 1 to 5
	 	v1 <- 1 to 5
	 	v2 <- 1 to 5
	 } yield (v0, v1, v2)                     //> res15: scala.collection.immutable.IndexedSeq[(Int, Int, Int)] = Vector((1,1
                                                  //| ,1), (1,1,2), (1,1,3), (1,1,4), (1,1,5), (1,2,1), (1,2,2), (1,2,3), (1,2,4)
                                                  //| , (1,2,5), (1,3,1), (1,3,2), (1,3,3), (1,3,4), (1,3,5), (1,4,1), (1,4,2), (
                                                  //| 1,4,3), (1,4,4), (1,4,5), (1,5,1), (1,5,2), (1,5,3), (1,5,4), (1,5,5), (2,1
                                                  //| ,1), (2,1,2), (2,1,3), (2,1,4), (2,1,5), (2,2,1), (2,2,2), (2,2,3), (2,2,4)
                                                  //| , (2,2,5), (2,3,1), (2,3,2), (2,3,3), (2,3,4), (2,3,5), (2,4,1), (2,4,2), (
                                                  //| 2,4,3), (2,4,4), (2,4,5), (2,5,1), (2,5,2), (2,5,3), (2,5,4), (2,5,5), (3,1
                                                  //| ,1), (3,1,2), (3,1,3), (3,1,4), (3,1,5), (3,2,1), (3,2,2), (3,2,3), (3,2,4)
                                                  //| , (3,2,5), (3,3,1), (3,3,2), (3,3,3), (3,3,4), (3,3,5), (3,4,1), (3,4,2), (
                                                  //| 3,4,3), (3,4,4), (3,4,5), (3,5,1), (3,5,2), (3,5,3), (3,5,4), (3,5,5), (4,1
                                                  //| ,1), (4,1,2), (4,1,3), (4,1,4), (4,1,5), (4,2,1), (4,2,2), (4,2,3), (4,2,4)
                                                  //| , (4,2,5), (4,3,1), (4,3,2), (4,3,3), (4,3,4), (4,3,5), (4,4,1), (4,4,2), (
                                                  //| 4,4,3), (4,4,4), (4,4,5
                                                  //| Output exceeds cutoff limit.
                                                  
	/*
	 * now filter it
	 */
         
	 for {
	 	v0 <- 1 to 5 if v0 % 2 != 0
	 	v1 <- 1 to 5 if v1 % 2 == 0
	 	v2 <- 1 to 5
	 } yield (v0, v1, v2)                     //> res16: scala.collection.immutable.IndexedSeq[(Int, Int, Int)] = Vector((1,2
                                                  //| ,1), (1,2,2), (1,2,3), (1,2,4), (1,2,5), (1,4,1), (1,4,2), (1,4,3), (1,4,4)
                                                  //| , (1,4,5), (3,2,1), (3,2,2), (3,2,3), (3,2,4), (3,2,5), (3,4,1), (3,4,2), (
                                                  //| 3,4,3), (3,4,4), (3,4,5), (5,2,1), (5,2,2), (5,2,3), (5,2,4), (5,2,5), (5,4
                                                  //| ,1), (5,4,2), (5,4,3), (5,4,4), (5,4,5))
	/*
	 * intermediate values
	 */
         
	 for {
	 	v0 <- 1 to 5 if v0 % 2 != 0
	 	v1 <- 1 to 5 if v1 % 2 == 0
	 	val v3 = v0 + v1
	 	v2 <- 1 to v3
	 } yield (v0, v1, v2)                     //> res17: scala.collection.immutable.IndexedSeq[(Int, Int, Int)] = Vector((1,2
                                                  //| ,1), (1,2,2), (1,2,3), (1,4,1), (1,4,2), (1,4,3), (1,4,4), (1,4,5), (3,2,1)
                                                  //| , (3,2,2), (3,2,3), (3,2,4), (3,2,5), (3,4,1), (3,4,2), (3,4,3), (3,4,4), (
                                                  //| 3,4,5), (3,4,6), (3,4,7), (5,2,1), (5,2,2), (5,2,3), (5,2,4), (5,2,5), (5,2
                                                  //| ,6), (5,2,7), (5,4,1), (5,4,2), (5,4,3), (5,4,4), (5,4,5), (5,4,6), (5,4,7)
                                                  //| , (5,4,8), (5,4,9))
	 
	/*
	 * reduction: obtain an 'aggregate' value from a collection of values (vector)
	 * eg. calculate max, min, mean ... of a collection
	 */
	  
	  
	/*
	 * example: calculate the sum of a collection, from scratch (but the method already exists... :) )
	 */
	   
	val sum = lst.foldLeft(0)((a, b) => a + b)//> sum  : Int = 6
		
	/*
	 * the foldLeft operator, starts from the beginning of the collection, and applies the specified 'aggregation' function (a, b) => a + b
	 * in our case) to element pairs. the '0' element is the 'neutral' element to the applied aggregation. it exists to allow to apply the foldLeft
	 * operator to lists with less than 2 elements
	 */
		 
	/*
	 * the result is not necessarily the same of type the container collection: it depends on the aggregation operation
	 */
		 
	lst.foldLeft("")((a, b) => a.toString + b.toString)
                                                  //> res18: String = 123
		  

	(1 to 20).toList.groupBy(x => x % 2 == 0) //> res19: scala.collection.immutable.Map[Boolean,List[Int]] = Map(false -> Lis
                                                  //| t(1, 3, 5, 7, 9, 11, 13, 15, 17, 19), true -> List(2, 4, 6, 8, 10, 12, 14, 
                                                  //| 16, 18, 20))



	/*
	 * sets
	 */

	val s = Set("key1", "key2", "key1")       //> s  : scala.collection.immutable.Set[String] = Set(key1, key2)



	/*
	 * insertions of already present values, returns unchanged set. So it does not crash if a key already exists
	 */

	s.map(v => v.substring(0, 3))             //> res20: scala.collection.immutable.Set[String] = Set(key)

	s + "key1"                                //> res21: scala.collection.immutable.Set[String] = Set(key1, key2)



	/*
	 * maps
	 */

	Map(("key1", 1), ("key2", 2))             //> res22: scala.collection.immutable.Map[String,Int] = Map(key1 -> 1, key2 -> 
                                                  //| 2)
                                                  
	/*
	 * syntactic sugar version, using alternate tuple initializer
	 */
	 
	Map("key1" -> 1, "key2" -> 2)             //> res23: scala.collection.immutable.Map[String,Int] = Map(key1 -> 1, key2 -> 
                                                  //| 2)
                 
	val m = Map("key1" -> 1, "key2" -> 2)     //> m  : scala.collection.immutable.Map[String,Int] = Map(key1 -> 1, key2 -> 2)
                                                  //| 

	/*
	 * EXERCISE
	 * EXERCISE
	 * EXERCISE
	 */
	
	/*
	 * group-by usage
	 *
	 * the groupBy method creates a map
	 *
	 * * tessellation
	 *
	 *   calculate the value of a given function z = f(x, y) in a quantized space x, y, where x [0, N - 1] and y [0, N - 1]
	 *
	 *   quantize the space by a sampling factor of Q (< N), and calculate the mean of z for all sub-region and assign it as a value for sub-region centroids
	 *
	 *   (can be thougt also as an image-resizing algorithm, but the image data is a function of (x, y))
	 */
	
	
	val N = 4                                 //> N  : Int = 4
	
	val Q = 2                                 //> Q  : Int = 2
	
	val z : (Int, Int) => Double = (x, y) => x * y
                                                  //> z  : (Int, Int) => Double = <function2>
	
	val space : Seq[(Int, Int)] = ???         //> scala.NotImplementedError: an implementation is missing
                                                  //| 	at scala.Predef$.$qmark$qmark$qmark(Predef.scala:225)
                                                  //| 	at seq$$anonfun$main$1.apply$mcV$sp(seq.scala:276)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$$anonfun$$exe
                                                  //| cute$1.apply$mcV$sp(WorksheetSupport.scala:76)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.redirected(W
                                                  //| orksheetSupport.scala:65)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.$execute(Wor
                                                  //| ksheetSupport.scala:75)
                                                  //| 	at seq$.main(seq.scala:3)
                                                  //| 	at seq.main(seq.scala)
	
	val groupedSpace : Map[(Double, Double), Seq[(Int, Int)]] = ???

                                                  
	val quantizedEvaluation : Map[(Double, Double), Double] = ???
}