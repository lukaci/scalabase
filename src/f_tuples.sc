import java.util.Random

object tuples {
	println("tuples basics")                  //> tuples basics

	/*
	 * tuples are implemented classes which allows to save time declaring temporary, intermediate classes
	 *
	 * they represents 'unnamed' sets of specialized arguments
	 */
	 
	val mytuple = ("luka", "ci")              //> mytuple  : (String, String) = (luka,ci)
	
	val mytuple2 = Tuple2("luka", "ci")       //> mytuple2  : (String, String) = (luka,ci)
	
	val mytuple3 = "luka" -> "ci"             //> mytuple3  : (String, String) = (luka,ci)
	
	/*
	 * tuples members can be accessed by the ._X members
	 */
	 
	mytuple._1                                //> res0: String = luka
	 
	 
	/*
	 * tuple arguments can be iterated (without reflection!) and accessed by field index, at runtime
	 */
	  
	for (a <- mytuple.productIterator) println(a)
                                                  //> luka
                                                  //| ci
                                                  
	mytuple.productElement(1)                 //> res1: Any = ci
	
	
	/*
	 * special tuple decomposition: sometimes is useful to compactly expand tuple members into vals
	 */
	 
	def functionWithTupleResult = {
		val num = new Random().nextInt
		val str = "random"
		
		(str, num)
	}                                         //> functionWithTupleResult: => (String, Int)
	
	val c = functionWithTupleResult           //> c  : (String, Int) = (random,2134720789)
	
	val (str, num) = functionWithTupleResult  //> str  : String = random
                                                  //| num  : Int = -1675598268
}