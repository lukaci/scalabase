import java.util.Random

object opt {
	println("options basics")                 //> options basics

	/*
	 * options are the best way to avoid null references!
	 *
	 * options represents an optional generic value container, which can exist or not
	 *
	 * in fact, an option is a trait with 2 implementations
	 *  - None: the value is not specified (such as nulls)
	 *  - Some( value ): the value is defined
	 */
	
	val c = None                              //> c  : None.type = None
	val d = Some(1)                           //> d  : Some[Int] = Some(1)
	
	/*
	 * options have methods to check if value exists and to get its value
	 */
	 
	def checkOption(o: Option[Int]) = {
		if(o.isDefined) println("IS DEFINED, its value is: " + o.get)
		else println("IS NOT DEFINED")
	}                                         //> checkOption: (o: Option[Int])Unit
	
	checkOption(None)                         //> IS NOT DEFINED
	
	checkOption(Some(1))                      //> IS DEFINED, its value is: 1
	
	/*
	 * if we try to get an option value, when the option is None, an exception (NoSuchElementException) will be thrown
	 */
	 
	// None.get
	
	/*
	 * having a data structure representing that the value can be optional, allows developers to REMEMBER that the value can not be set.
	 * it is a good practice to use options instead of nulls..we can decide to 'feel lucky' and get the option value without value check
	 * (thus converting NullPointerException to NoSuchElementException) or handle its optionality by performing checks
	 */
	 
	 
	/*
	 * sometimes it can be awkward to check everytime the presence of an option value using 'if'..there are other methods
	 *
	 * for example, if we have to convert an optional integer to a string, we have to check its presence. Then, convert it to string if present, else
	 *   do something. So the caller of a function that perform this work should receive an option to apply itself the right decision
	 *
	 * we can simply transform an option to another option!
	 */
	 
	 val s = Some(10).map(v => v.toString)    //> s  : Option[String] = Some(10)
	 
	/*
	 * we can decide to do something if an option is not set...for example, returning a default value
	 */
	
	val o2 : Option[Int] = None               //> o2  : Option[Int] = None
	
	val integer = o2.getOrElse(0)             //> integer  : Int = 0

	
	def checkOption2(o: Option[Int]) = {
		if(o.isDefined) println("IS DEFINED, its value is: " + o.get)
		else println("IS NOT DEFINED")
	}                                         //> checkOption2: (o: Option[Int])Unit
	
	checkOption2(None)                        //> IS NOT DEFINED
	
	checkOption2(Some(1))                     //> IS DEFINED, its value is: 1
	
	
	/*
	 * we can combine more options, to obtain an optional value depending on all options values
	 */
	 
	val one = None                            //> one  : None.type = None
	val two = None                            //> two  : None.type = None
	val three = Some(3)                       //> three  : Some[Int] = Some(3)
	 
	val result = one.orElse(two).orElse(three)//> result  : Option[Int] = Some(3)
	
	/*
	 * thinking about options as 1-len sequences: map, filter, flatmap...
	 */
	 
	 val aa = Some(1)                         //> aa  : Some[Int] = Some(1)
	 val bb = Some(2)                         //> bb  : Some[Int] = Some(2)
	 val cc = Some(3)                         //> cc  : Some[Int] = Some(3)
	 
	 for {
	 	a <- aa
	 	b <- bb
	 	c <- cc
	 } yield true                             //> res0: Option[Boolean] = Some(true)
	 
	 	
	/*
	 * and if we have a list of options, we want al values that are set
	 */
	 
	val optionList = List(Some(1), Some(2), None, None, Some(4))
                                                  //> optionList  : List[Option[Int]] = List(Some(1), Some(2), None, None, Some(4
                                                  //| ))
	
	val filter1 = optionList.filter(v => v.isDefined).map(v => v.get)
                                                  //> filter1  : List[Int] = List(1, 2, 4)
	
	/*
	 * or, in a straightforward way...
	 */
	 
	val filter2 = optionList.flatten          //> filter2  : List[Int] = List(1, 2, 4)
}