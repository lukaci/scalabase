import java.util.Random

object objects {
	println("objects basics")

	/*
	 * objects are singleton instances of classes, defined in the object definition itself
	 */
		 
	object example {
		def ex = println("this is an example")
	}
	
	example.ex
	
	
	
	class person(val name: String) {
		def greet = println("hello, my name is " + name)
	}
	 
	object pippo extends person("pippo")

	pippo.greet


	
	/*
	 * special objects are 'companion' objects, which are objects with same name of a class/trait
	 *
	 * they are perfect for factory methods
	 */
	 
	object person {
		val available = List("pippo", "pluto", "paperino")
		 
		def apply(name: String) = {
			new person(name)
		}
		
		def apply(index: Int) = {
			new person(available(index))
		}
	}
	
	val first = person("firstperson")
	val inlist = person(2)
	
	first.name
	
	inlist.name
}