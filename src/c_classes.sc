import java.util.Random

object classes {
	println("classes basics")                 //> classes basics
	
	/*
	 * class methods are similar to functions
	 */
	
	class person {
		def greet = println("hi")
	}
	
	new person().greet                        //> hi


	
	/*
	 * the primary constructor is specified like a 'function' in the class declaration to save space
	 */
	 
	class man(message: String) {
		private val msg = message
		def greet = println(msg)
	}
	
	new man("hello").greet                    //> hello
	
	
	/*
	 * like in function declaration, constructor args are visible from within the whole class implementation
	 */
	
	class man2(message: String) {
		def greet = println(message)
	}
	
	
	
	/*
	 * imagine that we have a parameter that is used to fill a class property, and it is immutable...
	 */
	 
	class child(nameArg: String, message: String) {
		val name = nameArg
		
		def greet = println(message + " my name is " + name)
	}
	
	new child("pippo", "ciao").greet          //> ciao my name is pippo
	
	
	
	/*
	 * if we add val/var keyword in constructor arguments, they will be declared as public class members (properties)
	 *
	 * in shorter form we can write like this:
	 */
	
	class son(val name: String, message: String) {
		def greet = println(message + " my name is " + name)
	}
	
	new child("pippo", "hello").greet         //> hello my name is pippo
	
	
	/*
	 * we can write 'plain' classes (pure data structures), like immutable pojo/beans, with no effort
	 */
	 
	class personPojo(val name: String, val phone: String, val address: String)


	/*
	 * there is a special type of classes, which is 'case class'
	 *
	 * case class meke it even easier to declare data structures
	 *  - 'val' keyword can be omitted
	 *  - all constructor args are converted to members
	 *  - hashcode and equals are generated
	 */
	 
	case class personCaseClass(name: String, phone: String, address: String)
	
	/*
	 * case classes have other important features, we can discuss the next time
	 */
	

	
	/*
	 * additional constructors can be declared as well
	 */
	 
	class personAlt(val name: String, val surname: String) {
		def this(sname: String) { this("pippo", sname) }
	}


		/*
	 * inheritance: like you can imagine
	 */
	 
	class yourSon(surname: String) extends son("your", surname)
	
	
	
	/*
	 * abstract classes: like you can imagine
	 */
	 
	abstract class base(name: String) {
		def greet: Unit
	}
	
	class derivate(name: String) extends base(name) {
		def greet = println("hell, I am " + name)
	}
	
	new derivate("pluto").greet               //> hell, I am pluto
}