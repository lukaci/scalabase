import java.util.Random

object traits {
	println("traits basics")                  //> traits basics
	
	/*
	 * traits likes interface definitions, and can be used in that way
	 *
	 * they cannot be instantiated directly like interfaces and abstract classes
	 */
	 
	trait greeter {
		def greet: Unit
	}
	
	class me extends greeter {
		def greet = println("hi, it's me")
	}
	
	val g : greeter = new me                  //> g  : traits.greeter = traits$$anonfun$main$1$me$1@79cbf844
	
	g.greet                                   //> hi, it's me
	
	
	/*
	 * ... but traits can have implemented methods ...
	 *
	 * it is useful for external callers, as well as internal methods accessing trait base implementations (eg. extending a trait
	 *  which declares utility methods, allows a class to access that utilities, like a logger trait...
	 */
	 
	trait basegreeter extends greeter {
		def greet = println("hi, I'm the base")
	}
	
	class testgreeter extends basegreeter
	
	val t = new testgreeter                   //> t  : traits.testgreeter = traits$$anonfun$main$1$testgreeter$1@7df4591e
	
	t.greet                                   //> hi, I'm the base
	
	
	
	/*
	 * a class can inherit from one base class, but from MULTIPLE traits
	 *   but traits can have implementations
	 *   so we have multiple inheritance unlocked!
	 */
	 
	trait basestoryteller {
		def tell = println("once upon a time...")
	}
	
	class greetTeller extends basegreeter with basestoryteller {
		def listenTo = {
			greet
			tell
		}
	}
	
	val teller = new greetTeller              //> teller  : traits.greetTeller = traits$$anonfun$main$1$greetTeller$1@30ea06a
                                                  //| 1
	
	teller.listenTo                           //> hi, I'm the base
                                                  //| once upon a time...
                                                  
	/*
	 * traits can be attached to new object instances
	 */
	 
	val c = new Object with basegreeter       //> c  : traits.basegreeter = traits$$anonfun$main$1$$anon$2@32d5d62e
	 
	c.greet                                   //> hi, I'm the base
	
	
	
	/*
	 * dependency injection
	 */
	
	trait namedEntity {
		def name: String
	}

	trait nameTeller { this: namedEntity =>
		def tellName = println(this.name)
	}
	 
	class person(val name: String) extends namedEntity
	 
	
	val pt = new person("pippo") with nameTeller
                                                  //> pt  : traits.person with traits.nameTeller = traits$$anonfun$main$1$$anon$1
                                                  //| @747bc0aa
	
	pt.tellName                               //> pippo
	 
	
	/*
	 * scala prevents from mixing traits with overlapping method declarations (equal signatures)
	 */
	
	trait sample1 {
		def method : Int = 0
	}
	 
	trait sample2 {
		def method : Int = 1
	}
	 
	// this row throws a compile time error
	// val d = new Object with sample1 with sample2
}