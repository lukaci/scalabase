import java.util.Random

object functions {
	println("functions basics")               //> functions basics
	
	/*
	 * functions are a 'definition' of a computation
	 * imagine that we are DEFINING an equation:
	 *
	 *     first member = second member
	 */
 
	def anInt1() : Int = {
		val c = 1
		return c;
	}                                         //> anInt1: ()Int
	 
	 
	 
	/*
	 * semicolons is 'optional' (discouraged, used only when writing one-liner functions to preserve code compactness
	 *
	 * the 'return' statement is not needed, the returned value (except when specified by 'return' is the result of the last liine
	 *  of code in the function
	 */
	  
	def anInt2() : Int = {
		val c = 2
		c
	}                                         //> anInt2: ()Int
	 
	 	
	 	
	/*
	 * type inference: the return type (if 'simple' to be inferred...) can be avoided. It is often avoided for simple functions
	 * but can be explicited for clarity and readability
	 *
	 * round brackets for empty parameter-list function can be omitted
	 *
	 * curly brackets for one-liner, single expression functions, can be omitted
	 */
	 	 
	def anInt3 = 3                            //> anInt3: => Int
	  
	/*
	 * type inference works well! (not only in functions) it finds the best type which is a superclass of
	 * all possible results
	 */
	  
	def something = {
		if(true) false
		else 3.0
	}                                         //> something: => AnyVal
	  
	/*
	 * functions can be nested
	 * inner functions are accessible only from within the container function (like java non-static inner classes)
	 */

	def anInt4 = {
  	def intoAnInt4 = {
  		val c = 4
  		4
  	}
  	
  	intoAnInt4
  }                                               //> anInt4: => Int











	println("parameterized functions")        //> parameterized functions
		

	/*
	 * we can write simple 'equations'
	 */
	 
	def sum(a: Int, b: Int) = a + b           //> sum: (a: Int, b: Int)Int
	  
	/*
	 * function arguments are accessible from within the function
	 */
	 
	def compare(a: String, b: String, padding:String) = {
		 def pad(s: String, pc: String) = {
		 	s.replaceAll(pc, "")
		 }
		 
		 pad(a, padding) == pad(b, padding)
	}                                         //> compare: (a: String, b: String, padding: String)Boolean
	  
	/*
	 * the 'padding' argument is equal for all calls to the inner 'pad' function, wecan avoid passing it
	 *
	 * ... so we can save parameters and add compactness to our implementation
	 *
	 * in the case of equal-named arguments, the 'nearmost' is the winner
	 */
	 
	def compare2(a: String, b: String, padding:String) = {
		def pad(s: String) = {
			s.replaceAll(padding, "")
		}
		 
		pad(a) == pad(b)
	}                                         //> compare2: (a: String, b: String, padding: String)Boolean
	 
	
	compare("  pippo  ", "pippo", " ")        //> res0: Boolean = true
	compare2("  pippo  ", "pippo", " ")       //> res1: Boolean = true
	
	
	
	/*
	 * in scala, there is no 'void' data type. everything in scala is a function, that returns a value
	 * there is an 'assignable' equivalent of 'void' in scala, which is the Unit type, that can be assigned (void is not)
	 *
	 * in this way, everything can be 'transformed' calling something
	 *
	 * this is valid also for 'control structures' like if-else, for, while, ...
	 */

	def transform(a: Int, threshold: Int) = {
		val res = if(a > threshold) a*a
							else a/2
		
		res
	}                                         //> transform: (a: Int, threshold: Int)Int


	/*
	 * functions in scala are first-class citizens
	 *
	 * functions can be teated as other data types
	 *
	 * a function has its own type, which is defined by the type of its arguments and the return type
	 *
	 * its form is explicit: defines a transformation from a domain (arguments) to another domain (return value)
	 *
	 *     (from) => to
	 */
	
	def truncate(num: Double) : Int = {
		println("inside truncate")
		num.toInt
	}                                         //> truncate: (num: Double)Int
	
	/*
	 * note that we are not assigning the result of a function, but the function itself
	 */
	
	val truncate2 : Double => Int = truncate  //> truncate2  : Double => Int = <function1>
	
	
	
	truncate(1.5)                             //> inside truncate
                                                  //| res2: Int = 1
	
	truncate2(1.5)                            //> inside truncate
                                                  //| res3: Int = 1
	
	
	/*
	 * so...we can pass functions as arguments? YES
	 */
  
  def sumSquares(a: Int, b: Int) = {
  	a*a + b*b
  }                                               //> sumSquares: (a: Int, b: Int)Int
  
  def sumCubes(a: Int, b: Int) = {
  	a*a*a + b*b*b
  }                                               //> sumCubes: (a: Int, b: Int)Int
  
  /*
   * we can use it, for example, to generalize a function, 'injecting' some logic as an argument
   */
   
	def square(a: Int) = a*a                  //> square: (a: Int)Int
	def cube(a: Int) = a*a*a                  //> cube: (a: Int)Int
   
	def sumTransformed(a: Int, b:Int, transform: Int => Int) = transform(a) + transform(b)
                                                  //> sumTransformed: (a: Int, b: Int, transform: Int => Int)Int

	/*
	 * so an alternative implementation of our sumSquares and sumCubes is...
	 */
	
	def sumSquares2(a: Int, b: Int) = sumTransformed(a, b, square)
                                                  //> sumSquares2: (a: Int, b: Int)Int
	def subCubes2(a: Int, b: Int) = sumTransformed(a, b, cube)
                                                  //> subCubes2: (a: Int, b: Int)Int


	/*
	 * we can also declare 'anonymous' functions when we need them
	 */

	def sumSquares3(a: Int, b: Int) = sumTransformed(a, b, (x: Int) => {x*x})
                                                  //> sumSquares3: (a: Int, b: Int)Int

	/*
	 * we can omit the 'unwanted' boilerplate separators, types are inferred ... then
	 */

	def sumSquares4(a: Int, b: Int) = sumTransformed(a, b, x => x * x)
                                                  //> sumSquares4: (a: Int, b: Int)Int


	/*
	 * a 'special' case of functions passed as arguments can be the 'call by value' argument passing mode
	 *
	 * by default, scala uses the 'call by value' model, which 'collects' all the arguments before executing function code
	 *
	 * the 'call by name' model instead lazily collects argument values when needed
	 */

	def callByValue(a: Int, b: Int) = {
		println("sum #1: " + (a + b))
		println("sum #2: " + (a + b))
	}                                         //> callByValue: (a: Int, b: Int)Unit
  
  /*
   * the syntax of teclaring callByName arguments recalls passing functions as arguments.
   *
   * we can identify a and b args as 'functions' which input arguments are not mentioned, but we only know
   *   that can return a value.
   */
  
	def callByName(a: => Int, b: => Int) = {
		println("sum #1: " + (a + b))
		println("sum #2: " + (a + b))
	}                                         //> callByName: (a: => Int, b: => Int)Unit
	
	callByValue(new Random().nextInt, new Random().nextInt)
                                                  //> sum #1: 1735913947
                                                  //| sum #2: 1735913947
	callByName(new Random().nextInt, new Random().nextInt)
                                                  //> sum #1: -159794452
                                                  //| sum #2: 176536371



	/*
	 * so...we can return functions from functions? YES
	 *
	 * think it different, imagine that we can return a 'specialized' function
	 *
	 * in this case, mulTransformed returns a specialized function which applies the given transformation and performs the multiply op
	 */
	
	def mul(a: Int, b: Int) = a * b           //> mul: (a: Int, b: Int)Int
	
	def mulTransformed(transform: Int => Int) : (Int, Int) => Int = {
		def specializedMulTransformed(a: Int, b: Int) = {
			mul(transform(a), transform(b))
		}
		
		specializedMulTransformed
	}                                         //> mulTransformed: (transform: Int => Int)(Int, Int) => Int
	 
	 
	 
	/*
	 * now, to use this function, we have first to make a call to mulTransformed, then use the resulting function with parameters
	 */
	 
	 def mulSquares = mulTransformed(square)  //> mulSquares: => (Int, Int) => Int
	 
	 mulSquares(2, 2)                         //> res4: Int = 16
	
	
	/*
	 * but, in the real world, it is easier to return functions.
	 *
	 * imagine the operation we did to obtain previous result of mulSquares(2, 2)
	 */
	 
	 (mulTransformed(square))(2,2)            //> res5: Int = 16
	  
	/*
	 * so     mulSqares(2, 2)    ==    (mulTransformed(square))(2, 2)
	 *
	 *
	 * in Scala, there is a syntax which helps writing functions that returns functions, called 'currying' (Haskell Brooks Curry)
	 */
	 
	def mulTransformed2(transform: Int => Int)(a: Int, b: Int) = {
	 	/* the cool thing is that we haven't to explicitly create a subfunction.
	 	   this way of definitions allows to use all parameters from the multiple parameters lis, so we can
	 	   directly write the final implmentation of the result
	 	*/
	 	
	 	mul(transform(a), transform(b))
	}                                         //> mulTransformed2: (transform: Int => Int)(a: Int, b: Int)Int
	
	/*
	 * which is the type of this function?
	 *
	 * it is a function which accepts a function Int => Int and returns another function, which is (Int, Int) => Int
	 *   so it is (Int => Int) => (Int, Int) => Int
	 */
	 
	 
	 
	/*
	 * partially applied functions: we can obtain an half-specialized function by specifying a subset of a function arguments
	 */
	 
	 def sum2 = sum(2, _:Int)                 //> sum2: => Int => Int
	 
	 // def mulTransformed2 = mulTransformed(square)(_:Int, 2)
	 
	 // and more
	 
	 
	 
	/*
	 * variable arguments
	 */
	  
	def varargs(args: String*) = {
		for(s <- args)
			println(s)
	}                                         //> varargs: (args: String*)Unit
	
	
	varargs("a", "b", "c")                    //> a
                                                  //| b
                                                  //| c
	
	 
	println("it is only the beginning!")      //> it is only the beginning!
}